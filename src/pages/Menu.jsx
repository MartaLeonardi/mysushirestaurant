import { useState, useEffect } from "react"
import Card from "../components/Card/Card"

import california from "./../images/california.png"
import dragon from "./../images/dragon.png"
import dynamite from "./../images/dynamite.png"
import philadelphia from "./../images/philadelphia.png"
import rainbow from "./../images/rainbow.png"
import shrimp from "./../images/shrimp.png"

export default function Menu (){

    useEffect(() => {
        document.title = "Menu";
      }, []);

const [state, setDishState] = useState(
    {
        cards:[
            {id: 0, immagine: california, nomePiatto: "California", prezzo:1.99, quantita: 0},
            {id: 1, immagine: dragon, nomePiatto: "Dragon", prezzo:3.50, quantita: 0},
            {id: 2, immagine: dynamite, nomePiatto: "Dynamite", prezzo:2.99, quantita: 0},
            {id: 3, immagine: philadelphia, nomePiatto: "Philadelphia", prezzo:1.99, quantita: 0},
            {id: 4, immagine: rainbow, nomePiatto: "Rainbow", prezzo:4.99, quantita: 0},
            {id: 5, immagine: shrimp, nomePiatto: "Shrimp", prezzo:1.99, quantita: 0},
        ]
    }

)

    //funzione per decrementare la quantita del piatto selezionato
    const handleDecrement = cardId  => {
        const updatedCards = state.cards.map(card => {
            if (card.id === cardId && card.quantita>0) {
                return { ...card, quantita: card.quantita - 1 };
            }
            return card;
        });
        setDishState({ cards: updatedCards });
    }

    //funzione per incrementare la quantita del piatto selezionato
    const handleIncrement = cardId => {
        const updatedCards = state.cards.map(card => {
            if (card.id === cardId) {
                return { ...card, quantita: card.quantita + 1 };
            }
            return card;
        });
        setDishState({ cards: updatedCards });
    }

    return (
        <>
        <div className="container text-center" style={{padding:"1%"}}>
                <h1>Il nostro menu!</h1>
            <div className="row align-items-center justify-content-center">

        {/*Genero tutte le carte ciclicamente attraverso la funzione map */}
                {state.cards.map(card => (
                    <Card
                    key={card.id}
                    card={card}
                    onIncrement={()=>handleIncrement(card.id)}
                    onDecrement={()=>handleDecrement(card.id)}
                    ></Card>
                ))}

            </div>
            </div>
        </>
    )
}