import RegisterForm from "../components/RegisterForm/RegisterForm";
import { useEffect } from "react";

export default function Register (){

    useEffect(() => {
        document.title = "Registrazione";
      }, []);

return(
    <>
        <div className="container justify-content-center" style={{padding:"5%", display: "flex"}}>
            <RegisterForm></RegisterForm>
        </div>
    </>
)

    }
