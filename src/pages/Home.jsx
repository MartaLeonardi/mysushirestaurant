import Carousel from "../components/Carousel/Carousel"
import logo from '../images/logo.png'
import '../components/style.css'
import { useEffect } from "react";
import './style/home.css'

export default function Home(){

    useEffect(() => {
        document.title = "Home";
      }, []);
    

    return(
        <>
            <div className="container text-center" style={{padding:"1%"}}>

<div>
                <img src={logo} alt="logo" id="logoImgHome"/>
                <h1>mySushiRestaurant</h1>
                <img src={logo} alt="logo" id="logoImgHome" style={{display:"inline-block"}}/>
</div>
            
                

            <div className="row align-items-center">
                <Carousel/>
            </div>
            </div>
        
        </>
    )
}
