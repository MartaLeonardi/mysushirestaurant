import css from "../Footer/Footer.module.css";

export default function Footer() {
  return (
    <>
      <div className={`${css.myFooter} container-fluid`}>
        <nav>
          <div className="container text-center">
            <div className="row align-items-start">
              
              <div className="col">
                <h5>Dove siamo:</h5>
                <p>Via MySushiRestaurant, n° 100</p>
              </div>
              <div className="col">
                <h6>I nostri contatti:</h6>
                <i className="bi bi-telephone-fill"></i>&ensp;
                <a type="tel">3319173015</a>
                <br />
                <i className="bi bi-envelope"></i>&ensp;
                <a type="email">leonardimarta@gmail.com</a>
              </div>
              <div className="col">
                <h6>I nostri social:</h6>
                <i className="bi bi-facebook"></i>&ensp;
                <a href="https://www.facebook.com/?locale=it_IT" className={`${css.socialLink}`}>Facebook</a>
                <br />
                <i className="bi bi-instagram"></i>&ensp;
                <a href="https://www.instagram.com/accounts/login/" className={`${css.socialLink}`}>Instagram</a>
                <br />
                <i className="bi bi-youtube"></i>&ensp;
                <a href="https://www.youtube.com/" className={`${css.socialLink}`}>Youtube</a>
              </div>
            </div>
          </div>
        </nav>
        <small>
          (Sushi)ta emozioni! &ensp;-- Copyright &copy; 2024 &ensp;&ensp;--
          @Author Marta Leonardi
        </small>
      </div>
    </>
  );
}
