import css from "../Card/Card.module.css"

export default function Card (props){
    return (
        <>
<div className={`${css.mySushiDish} card`} style={{width: "18rem"}}>
  <img src={props.card.immagine} className="card-img-top" alt="immagine del sushi roll"/>
  <div className="card-body">
    <h5 className="card-title">{props.card.nomePiatto} roll</h5>
    <h6 className="card-subtitle mb-2 text-body-secondary">€{props.card.prezzo}</h6>

      {true ?(

        <>
          <button type="button" className="btn btn-lg"><i className="bi bi-dash-square-fill" onClick={() => props.onDecrement(props.card)}/></button>
          <span className="w3-badge w3-margin-left w3-white">{props.card.quantita}</span>
          <button type="button" className="btn btn-lg"><i className="bi bi-plus-square-fill" onClick={() => props.onIncrement(props.card)}/></button>
        </>
          
      ) : (
        <></>
      )}

  </div>
</div>
        </>
    )
}
