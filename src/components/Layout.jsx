import { useOutlet } from "react-router-dom";
import Navbar from "./Navbar/Navbar";
import Footer from "./Footer/Footer";


export default function Layout(){

    const outlet = useOutlet();

    return (
        <>
        <div>
            <Navbar/>
            <div style={{backgroundColor: "#94B392"}}>
                {outlet}
            </div>
            
            <Footer />
        </div>
        </>
    );
}